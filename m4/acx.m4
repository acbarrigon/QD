## Copyright (C) 2002 M. Marques, A. Castro, A. Rubio, G. Bertsch
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
## 02111-1307, USA.
##
## $Id: acx.m4 2166 2006-05-29 22:04:31Z athimm $
##



################################################
# AC_LANG_FUNC_LINK_TRY(Fortran)(FUNCTION)
# ----------------------------------
m4_define([AC_LANG_FUNC_LINK_TRY(Fortran)],
[AC_LANG_PROGRAM([], [call [$1]])])

################################################
# AC_LANG_PREPROC(Fortran)
# ---------------------------
m4_define([AC_LANG_PREPROC(Fortran)],[
  # this should not be hardwired
  if test -z "$FCCPP"; then FCCPP="/lib/cpp -C -ansi"; fi
  AC_SUBST(FCCPP)
])

################################################
# Get default FFLAGS
# ----------------------------------
# this function can certainly be improved on
AC_DEFUN([ACX_FCFLAGS],
[
AC_REQUIRE([AC_CANONICAL_HOST])

if test -z "${FCFLAGS}"; then
  case "${host}" in
  i?86*linux*)
    case "${FC}" in
    pgf90*)
      FCFLAGS="-O2 -fast -Munroll -Mnoframe -Mdalign"
      ;;
    abf90*)
      FCFLAGS="-O -YEXT_NAMES=LCS -YEXT_SFX=_"
      ;;
    ifc|ifort*)
      FCFLAGS="-u -zero -fpp1 -nbs -pc80 -pad -align -unroll -O3 -ip"
      a=`echo $host | sed "s/^i//" | sed "s/86*//"`
      if test $a > 5 ; then
         FCFLAGS="$FCFLAGS -tpp7 -xW"
      fi
    ;;
    *)
      FCFLAGS="-O"
    esac
    ;;
  x86_64*)
    dnl NAG => FCFLAGS="-colour -kind=byte -mismatch_all -abi=64 -ieee=full -O4 -Ounroll=4"
    dnl ABSOFT => FCFLAGS="-O3 -mcmodel=medium -m64 -cpu:host -YEXT_NAMES=LCS -YEXT_SFX=_ -YDEALLOC=MINE"
    dnl PGI => FCFLAGS="-fastsse -mcmodel=medium -O4 -Mdalign -Mlarge_arrays -Mscalarsse -Munroll=c:4,n:4 -Mvect=assoc,sse,cachesize:262144 -Minfo"
    ;;
  alphaev*)
    FCFLAGS="-align dcommons -fast -tune host -arch host -noautomatic"
    ;;
  powerpc-ibm*)
    FCFLAGS="-bmaxdata:0x80000000 -qmaxmem=-1 -qsuffix=f=f90 -Q -O5 -qstrict -qtune=auto -qarch=auto -qhot -qipa"
    ;;
  mips-sgi-irix*)
    FCFLAGS="-O3 -INLINE -n32 -LANG:recursive=on"
    ;;
  *)
    FCFLAGS="-O"
  esac
fi
AC_MSG_NOTICE([Using FCFLAGS="$FCFLAGS"])
])
