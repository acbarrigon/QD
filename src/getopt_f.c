/*
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2, or (at your option)
	any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
	02111-1307, USA.
*/

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#if defined(HAVE_GETOPT_LONG)
#include <getopt.h>
#else
#include <unistd.h>
#endif
#include <string.h>
#include "string_f.h"

/* GENERAL FUNCTIONS AND VARIABLES */

char **argv;
int argc;

void FC_FUNC_(set_number_clarg, SET_NUMBER_CLP)(int *nargc)
{
  argc = *nargc+1;
  argv = (char **)malloc(argc*sizeof(char *));
}

void FC_FUNC_(set_clarg, SET_CLARG)(int *i, STR_F_TYPE arg STR_ARG1)
{
  char *c;
  TO_C_STR1(arg, c)
  argv[*i] = c;
}

void FC_FUNC_(clean_clarg, CLEAR_CLARG)()
{
  int i;
  for(i=0; i<argc; i++)
    free(argv[i]);
  free(argv);
}


/* FUNCTIONS TO BE USED BY THE PROGRAM oct-oscillator-strength */
void qd_help(){
  printf("Usage: qd [OPTIONS]\n");
  printf("\n");
  printf("Options:\n");
  printf("  -h, --help               Print this help and exits.\n");
  printf("  -v, --version            Prints qd version.\n");
  printf("  -c, --coefficients       Generates coefficients for the discretization.\n");
  printf("  -p, --test_hartree       Tests the Poisson solver.\n");
  printf("  -l, --test_laplacian     Tests the Laplacian.\n");
  printf("  -e, --test_exponential   Tests the exponential.\n");
  printf("  -g, --gs                 Performs a ground state calculation.\n");
  printf("  -t, --td                 Performs a time-dependent calculation.\n");
  printf("  -s, --strength_function  Calculates the strength function.\n");
  printf("  -x, --excitations        Performs a LR-TDDFT calculation.\n");
  exit(-1);
}


void FC_FUNC_(getopt_qd, GETOPT_QD)
     (int *mode){

  int c;

  /* If there are no arguments, then just prints the copyright info and exits.*/
  if(argc==1){
    printf("qd %s\n", PACKAGE_VERSION);
    printf("Written by The 2014 Benasque TDDFT School.\n\n");
    printf("Copyright (C) 2014 The 2014 Benasque TDDFT School\n");
    printf("This program is free software; you may redistribute it under the terms of\n");
    printf("the GNU General Public License. This program has absolutely no warranty.\n");
    exit(0);
  } 

#if defined(HAVE_GETOPT_LONG)
  static struct option long_options[] =
    {
      {"help", no_argument, 0, 'h'},
      {"version", no_argument, 0, 'v'},
      {"coefficients", no_argument, 0, 'c'},
      {"test_hartree", no_argument, 0, 'p'},
      {"test_laplacian", no_argument, 0, 'l'},
      {"test_exponential", no_argument, 0, 'e'},
      {"gs", no_argument, 0, 'g'},
      {"td", no_argument, 0, 't'},
      {"strength_function", no_argument, 0, 's'},
      {"excitations", no_argument, 0, 'x'},
      {0, 0, 0, 0}
    };
#endif

  while (1) {
    int option_index = 0;
#if defined(HAVE_GETOPT_LONG)
    c = getopt_long(argc, argv, "hvcplegtsx", long_options, &option_index);
#else
    c = getopt(argc, argv, "hvcplegtsx");
#endif
    if (c == -1) break;
    switch (c) {

    case 'h':
      qd_help();
      break;

    case 'v':
      printf("qd %s\n", PACKAGE_VERSION);
      exit(0);

    case 'c':
      *mode = 1;
      break;

    case 'p':
      *mode = 2;
      break;

    case 'l':
      *mode = 3;
      break;

    case 'e':
      *mode = 4;
      break;

    case 'g':
      *mode = 5;
      break;

    case 't':
      *mode = 6;
      break;

    case 's':
      *mode = 7;
      break;

    case 'x':
      *mode = 8;
      break;

    default:
      exit(-1);

    }
  }

}


