/*
 Copyright (C) 2002 M. Marques, A. Castro, A. Rubio, G. Bertsch

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 02111-1307, USA.
*/


#include <stdio.h>
#include <math.h>
#include <gsl/gsl_sf_bessel.h>

#include "system.h"

/* Fortran wrappers */
#define BESSEL_FC FC_FUNC (bessel, BESSEL)
#define BESSELI_FC FC_FUNC (besseli, BESSELI)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif


double BESSEL_FC
     (int *n, double *x)
{
  return gsl_sf_bessel_Jn(*n, *x);
}

double BESSELI_FC
(int *n, double *x)
{
  return gsl_sf_bessel_In(*n, *x);
}
