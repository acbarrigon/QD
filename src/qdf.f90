!! Copyright (C) 2014 The 2014 Benasque TDDFT School
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
!! 02111-1307, USA.

program main
  use command_line_m

  integer :: mode
  integer :: ierr

  ! First, we read the command line, which is done by making use of the 
  ! subroutines in the command_line_m module.
  call getopt_init(ierr)
  if(ierr.ne.0) then
    write(0, *) 'Command line arguments cannot be read. Abort.'
    stop
  end if
  ! The default will be run mode 1: calculation of the coefficients:
  mode = 1
  call getopt_qd(mode)
  call getopt_end()

  ! Now, we run the appropriate code, that depends on the calculation mode
  ! read in the command line.
  select case(mode)
    case(1); call coeff
    case(2); call test_hartree
    case(3); call test_laplacian
    case(4); call test_exp
    case(5); call gs
    case(6); call td
    case(7); call strength_function
    case(8); call excitations
  end select


end program main

